Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
    confirmations: 'devise_overrides/confirmations',
    passwords: 'devise_overrides/passwords',
  }

  get '/' => 'etc#splash'
  get '/privacy' => 'etc#privacy'
  get '/count' => 'etc#count'
  get '/error_logs/:front_or_back' => 'etc#error_logs'

  get '/users/confirmed' => 'users#confirmed'
  resources :users, only: [:update, :show]
  get '/users/:id/closest_five_new' => "users#closest_five_new"
  get '/users/:id/closest_new' => "users#closest_new"

  resources :images, only: [:create, :destroy]
  get '/images/profile_images/:user_id' => 'images#profile_images'

  resources :reports, only: [:create]

  resources :matches, only: [:show, :create]
  get '/matches/:id/:match_user_id' => 'matches#match_profile'
  patch '/matches/deactivate' => 'matches#deactivate'
  patch '/matches/dislike' => "matches#dislike"

  mount ActionCable.server => '/cable'

  get '/messages/:user_id/:match_id(/:chat_screen)' => 'messages#list'

  resources :notifications, only: [:show]

  resources :password_reset, only: [:index]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  post '/error_log' => 'error_log#update'
end
