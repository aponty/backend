system("brew services start redis") unless Rails.env.production?

at_exit do
    begin
        puts 'Flushing Redis...'
        Redis.new.flushall
    rescue => e
        puts "There was an #{e.to_s} while flushing redis..."
    ensure
        puts 'Done Flushing Redis!'
    end
end unless Rails.env.production?
