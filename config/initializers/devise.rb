Devise.setup do |config|
    # The e-mail address that mail will appear to be sent from
    # If absent, mail is sent from "please-change-me-at-config-initializers-devise@example.com"
    config.mailer_sender = "spot.catch.dev@gmail.com"
    config.password_length = 6..128
    
    # don't lock access from unconfirmed accounts...for now
    config.allow_unconfirmed_access_for = nil
    
    # ==> ORM configuration
    # Load and configure the ORM. Supports :active_record (default) and
    # :mongoid (bson_ext recommended) by default. Other ORMs may be
    # available as additional gems.
    require 'devise/orm/active_record'
  
    # If using rails-api, you may want to tell devise to not use ActionDispatch::Flash
    # middleware b/c rails-api does not include it.
    # See: https://stackoverflow.com/q/19600905/806956
    config.navigational_formats = [:json]
end
