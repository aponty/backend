
class MessageBroadcastJob < ApplicationJob
    queue_as :default
  
    def perform(message)
      payload = {
        conversation_id: message.conversation.id,
        body: message.body,
        user_id: message.user_id,
        created_at: message.created_at,
      }
      ActionCable.server.broadcast(room_id(message), payload) 
    end

    def room_id(message)
      puts "chat:#{message.conversation.to_gid_param}"
      room_id ||= "chat:#{message.conversation.to_gid_param}"
    end
  end
