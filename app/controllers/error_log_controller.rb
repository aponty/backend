class ErrorLogController < ApplicationController
    def update
        begin
            File.open('log/frontend_production.log', 'a') do |f|
                f.puts error_params[:error]
                f.puts error_params[:stacktrace]
                f.puts "
                
                LOG_END
                
                "
            end
            head :ok
        rescue
            head :bad_request
        end
    end

    def error_params
        @error_params ||= params.permit(:error, :stacktrace)
    end
end
