class DeviseOverrides::ConfirmationsController < DeviseTokenAuth::ConfirmationsController	
	def resource_params
		# added config and redirect just to silence unpermitted params warning
		params.permit(:email, :confirmation_token, :config_name, :config, :redirect_url, :_method, :reset_password_token)
	end
end
