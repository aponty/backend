class DeviseOverrides::PasswordsController < DeviseTokenAuth::PasswordsController
    def resource_params
        # added config and redirect just to silence unpermitted params warning
        params.permit(:email, :reset_password_token, :config, :redirect_url, :password, :password_confirmation, :_method)
    end

    def render_edit_error
        render plain: "Reset links are only valid once - please request another!"
    end

    def render_update_success
        render plain: "Your password has been successfully updated! Please try logging in again"
    end
end
