class ApplicationController < ActionController::API
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :configure_permitted_parameters, if: :devise_controller?
    before_action :authenticate_user!
    
    AUTHENTICATE_USER_EXCEPT_CONTROLLERS = [
        "devise_token_auth/registrations", 
        'devise_token_auth/sessions', 
        'devise_overrides/confirmations',
        'devise_overrides/passwords',
        'password_reset_index'
    ]

    protected

    def authenticate_user!
        unless AUTHENTICATE_USER_EXCEPT_CONTROLLERS.include?(params[:controller])
            super
        end
    end

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :config, :redirect_url, :age, :gender])
    end
end
