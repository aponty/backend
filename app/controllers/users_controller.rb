class UsersController < ApplicationController
    skip_before_action :authenticate_user!, only: :confirmed
    
    def confirmed
        if user_params[:account_confirmation_success]
            render plain: "Flying monkeys deployed; evil robots are currently in retreat. Thank you!"
        else
            render plain: "Sorry, it looks like there was an error. Email spot.catch.dev@gmail.com if urgent!"
        end
    end

    def update
        data = user_params.except(:id, :notification_token)
        set_notification_token if fcm_notification_token.present?
        if user.update(data)
            head :created
        else 
            head :bad_request
        end
    end

    def show
        render json: user.profile
    end

    def closest_five_new
        render json: user.closest_n_new(5)
    end

    def closest_new
        if user.email == 'a@a.com'
            # for apple testing only, see note on n_new
            render json: user.n_new(1)
        else
            render json: user.closest_n_new(1)
        end
    end

    def user
        @user ||= User.find(user_params[:id])
    end

    def set_notification_token
        user_token_key = request.headers['client']
        user.tokens[user_token_key]["notification_token"] = fcm_notification_token
        user.tokens_will_change!
        # save/update not yet called
    end

    def fcm_notification_token
        @fcm_notification_token ||= user_params[:notification_token]
    end

    def user_params
        @user_params ||= params.permit(
            :id, :sport_grade, :trad_grade, :tr_grade, :boulder_grade, 
            :description, :sketch_rating, :last_latitude, :last_longitude, 
            :name, :account_confirmation_success, :notification_token,
            :age, :gender, :search_radius
        )
    end
end
