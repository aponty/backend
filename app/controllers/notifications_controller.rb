class NotificationsController < ApplicationController
    # handles internal notifications (red dots)
    # push notifications handled through fcm_push_notifier model
    def show
        has_notifications = new_matches.present? || user.has_unseen_messages
        render json: { has_notifications: has_notifications }
    end

    def new_matches
        UserConversation.where(user_id: user.id, user_has_seen: false)
    end

    def user
        @user ||= User.find(n_params[:id])
    end

    def n_params
        @n_params ||= params.permit(:id)
    end
end
