class MessagesController < ApplicationController
    def list
        render json: covo.serialized_messages
    end

    def convo
        @convo ||= Match.find_by(user_id: user_id, match_id: match_id).conversation
    end

    def user_id
        @user_id ||= messages_params[:user_id]
    end

    def match_id
        @match_id ||= messages_params[:match_id]
    end

    def messages_params
        @messages_params ||= params.permit(:user_id, :match_id, :chat_screen)
    end
end
