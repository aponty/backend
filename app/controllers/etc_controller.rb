class EtcController < ActionController::Base
    def splash
    end

    def privacy
    end

    def count
        render plain: User.count
    end

    def error_logs
        if frontend_errors
            render plain: File.open('log/frontend_production.log', 'a+'){|file| file.read }
        end
    end

    def frontend_errors
        etc_params[:front_or_back] == 'front'
    end
    
    def etc_params
        @etc_params ||= params.permit(:front_or_back)
    end
end
