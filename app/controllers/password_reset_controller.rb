class PasswordResetController < ActionController::Base
    def index
    end
    
    def password_reset_params
        @password_reset_params ||= params.permit(:password, :password_confirmation, :_method, :reset_password_token)
    end
end
