class MatchesController < ApplicationController
    def show 
        render json: user.match_profiles
    end

    def match_profile
        render json: user.match_profile(match, match_user)
    end

    def create
        match = user.match.build(match_id: match_user_id)
        if match.save && has_seen.save
            if match.is_mutual? 
                post_notification
            end
            render json: { new_mutual_match: match.is_mutual? }
        else
            head :bad_request
        end
    end

    def dislike
        if has_seen.save
            return head :created
        else
            head :bad_request
        end
    end

    def has_seen
        user.swiped_on.build(swiped_on_id: match_user_id)
    end

    def deactivate
        match =  Match.find_by("user_id = #{user_id} AND match_id = #{match_user_id} OR (user_id = #{match_user_id} AND match_id = #{user_id})")
        if match.deactivate
            head :ok
        else
            head :bad_request
        end
    end

    def post_notification
        match_user.notifier.post_match_notification(user)
    end

    def match
        @match ||= Match.find_by(user_id: user_id, match_id: match_user_id)
    end

    def match_user_id
        @match_user_id ||= matches_params[:match_user_id]
    end

    def match_user
        @match_user ||= User.find(match_user_id)
    end

    def user_id
        @user_id ||= matches_params[:id]
    end

    def user
        @user ||= User.find(user_id)
    end

    def matches_params
       @matches_params ||= params.permit(:id, :match_user_id, :other)
    end
end
