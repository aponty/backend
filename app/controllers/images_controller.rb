class ImagesController < ApplicationController

    def profile_images
        render json: Image.profile_images(user_id)
    end

    def create
        user_id = image_params[:user_id]
        image_key = image_params[:image_key][-1].to_i

        prior = Image.where(user_id: user_id, image_key: image_key)
        prior.destroy_all

        image_params[:image_key] = image_key
        image = Image.new(image_params)
        if image.save
            render json: image.id
        else
            head :bad_request
        end
    end

    def destroy
        image = Image.find(image_params[:id])

        if image.destroy
            head :ok
        else 
            head :bad_request
        end
    end

    def user_id
        @user_id ||= image_params[:user_id]
    end

    def image_params
        @image_params ||= params.permit(:image, :image_key, :user_id, :id)
    end
end
