class ReportsController < ApplicationController

    def create
        user = User.find(reports_params[:user_id])
        report_id = reports_params[:report_id]
        report = user.report.build(
            report_id: reports_params[:report_id],
            other_than_dating: reports_params[:other_than_dating],
        )
        match = Match.find_by("user_id = #{user.id} and match_id = #{report_id} or user_id = #{report_id} and match_id = #{user.id}")

        if report.save && match.deactivate
            head :created
        else
            head :bad_request
        end
    end

    def reports_params
        @reports_params ||= params.permit(:user_id, :report_id, :other_than_dating)
    end
end
