require 'fcm'

class FcmPushNotifier
    def initialize(user)
        @user = user
    end

    def post_message_notification(message)
        if notifications_enabled
            fcm.send(registration_ids, message_notification(message))
        end
    end

    def post_match_notification(match_user)
        if notifications_enabled
            fcm.send(registration_ids, match_notification(match_user))
        end
    end

    def registration_ids
        user_tokens = []
	    @user.tokens.each{|k, v| user_tokens.push(v['notification_token'])}
        user_tokens.compact
    end

    def notifications_enabled
        registration_ids.present?
    end

    private
    def fcm
        @fcm ||= begin
            server_key = Rails.application.credentials.FCM_SERVER_KEY
            FCM.new(server_key)
        end
    end

    private
    def match_notification(match_user)
        {
            collapse_key: 'match',
            notification: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                title: "You matched with #{match_user.name}!",
                body: "Go plan a climb :)",
            },
            data: {
                sender_id: match_user.id,
                type_key: 'match',
            }
        } 
    end
    
    private
    def message_notification(message)
        {
            collapse_key: 'message',
            notification: {
                click_action: "FLUTTER_NOTIFICATION_CLICK",
                title: "New message from #{message.sender.name}",
                body: message.body,
            },
            data: {
                sender_id: message.user_id,
                type_key: 'message',
            }
        }
    end
end
