class Message < ApplicationRecord
  belongs_to :conversation
  belongs_to :sender, class_name: :User, foreign_key: 'user_id'

  has_many :seens, dependent: :destroy
  
  validates_presence_of :body

  after_create :seen_by_sender
  after_create_commit { 
    MessageBroadcastJob.perform_later(self)
    post_message_notifications
  }

  def post_message_notifications
    users_to_notify = self.conversation.users.where.not(id: self.user_id)
    notifiers = users_to_notify.map(&:notifier)
    notifiers.each do |notifier|
        notifier.post_message_notification(self)
    end
  end

  def seen_by_sender
    self.seens.create(user_id: self.user_id)
  end

  def as_json(options={})
    super({methods: :seen_by}.merge(options))
  end
  
  def seen_by
    self.seens.pluck :user_id
  end
end
