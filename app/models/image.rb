class Image < ApplicationRecord
    belongs_to :user
    before_save :verify_and_resize

    def self.profile_images(user_id)
        images = Image.where(user_id: user_id)
        {
            image_0: images.find{|x| x[:image_key] == 0}&.encoded,
            image_1: images.find{|x| x[:image_key] == 1}&.encoded,
            image_2: images.find{|x| x[:image_key] == 2}&.encoded,
            icon: images.find{|x| x[:image_key] == 3}&.encoded,
        }
    end

    def self.icon(user_id)
        { icon: Image.find_by(user_id: user_id, image_key: 3)&.encoded }
    end

    def encoded
        return { 
            id: self.id,
            image: Base64.strict_encode64(self.image),
            image_key: self.image_key,
        }
    end

    def create_avatar
        old_avatar = Image.find_by(user_id: self.user_id, image_key: 3)
        new_binary = Lizard::Image.new(binary_image).resize(75, 75, :resize_down_only, 'jpeg').data

        if old_avatar.present?
            old_avatar.update!(image: new_binary)
        else
            Image.create!(
                image_key: 3,
                image: new_binary,
                user_id: self.user_id,
            )
        end
    end

    def is_avatar
        image_key == 3
    end

    def verify_and_resize
        return if is_avatar
        if Lizard::Image.is_image?(binary_image)
            if self.image_key == 0
                create_avatar
            end
            self.image = Lizard::Image.new(binary_image).resize(500, 500, :resize_down_only, 'jpeg').data
        else 
            throw :abort
        end
    end

    def binary_image
        @binary_image ||= Base64.strict_decode64(image)
    end
end
