class SwipedOn < ApplicationRecord
    belongs_to :user
    belongs_to :swiped_on, :class_name => "User"
end
