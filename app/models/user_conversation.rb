class UserConversation < ApplicationRecord
  belongs_to :user
  belongs_to :conversation

  def mark_seen
    update(user_has_seen: true)
  end

  def fetch_and_set_seen
    # marked seen after sent
    # possible to send and user closes app or whatever before seeing, but unlikely
    # better than waiting on a new read reciept being posted
    initial_val = user_has_seen
    mark_seen
    initial_val
  end
end
