# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  extend Devise::Models
  include DeviseTokenAuth::Concerns::User

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable,
         :confirmable

  acts_as_mappable :lat_column_name => :last_latitude,
                   :lng_column_name => :last_longitude

  has_many :images, dependent: :destroy
  
  has_many :user_conversation
  has_many :conversations, through: :user_conversation, dependent: :destroy

  has_many :messages, dependent: :destroy
  has_many :seens, dependent: :destroy

  has_many :match
  has_many :matches, through: :match, dependent: :destroy

  has_many :report
  has_many :reports, through: :report, dependent: :destroy

  has_many :swiped_on
  has_many :swiped_ons, through: :swiped_on, dependent: :destroy

  after_create_commit :match_with_spotbot

  def self.delete_user(user)
    Match.where(user_id: user.id).destroy_all
    Match.where(match_id: user.id).destroy_all
    SwipedOn.where(user_id: user.id).destroy_all
    SwipedOn.where(swiped_on_id: user.id).destroy_all
    Image.where(user_id: user.id).destroy_all
    UserConversation.where(user_id: user.id).destroy_all
    Seen.where(user_id: user.id).destroy_all
    user.destroy
  end

  def self.reset_Apple_test_account
    # need to delete all test accounts, and then re-create them with matches too
    test_user = User.find_by(email: 'a@a.com')
    User.delete_user(test_user)
    User.create(
      email: 'a@a.com', 
      password: 'password',
      name: "TEST USER 1",
      sketch_rating: 3,
      password_confirmation: 'password',
      last_latitude: -55.239854,
      last_longitude: -147.897290,
  )
  end

  def match_with_spotbot
    spot_bot = User.find_by(email: "spot.catch.dev@gmail.com")
    return if self == spot_bot || spot_bot.nil?
    match = self.match.create(match_id: spot_bot.id)
    self.swiped_on.create(swiped_on_id: spot_bot.id)
    inverse_match = spot_bot.match.create(match_id: self.id)
    welcome_mess = "Welcome to SpotCatch! Links ( https://bit.ly/3fYKkZ5 ) and 🙂🙃🙂 supported; group chat coming soon!"
    convo_id = match.conversation.id
    Message.create(body: welcome_mess, user_id: spot_bot.id, conversation_id: convo_id) 
  end

  def profile
    profile = self.as_json.except(*filter_from_public_profile)
    images = Image.profile_images(id)
    profile.merge(
      images: images,
      been_reported: been_reported?,
    )
  end

  def profile_lite
    profile = self.as_json.except(*filter_from_public_profile)
    profile.merge(
      images: Image.icon(id),
      been_reported: been_reported?,
    )
  end

  def n_new(n)
    # for apple testing. 
    # Don't know where their testing account will be located, 
    # this ensures test users aren't filtered out by distance
    User.find_by_sql([
      "SELECT users.* FROM users
      WHERE users.id != #{id}
      AND NOT EXISTS(
        SELECT 1 FROM swiped_ons 
        WHERE swiped_ons.swiped_on_id = users.id 
        AND swiped_ons.user_id = #{id}
      )
      LIMIT #{n}"
    ])
    .map{|user| user.profile}
  end

  def closest_n_new(n)
    nearby_ids = User.within(search_radius, origin: self).pluck(:id) - [id]
    return [] if nearby_ids.blank?
    User.find_by_sql([
        "SELECT users.* FROM users
        WHERE users.id IN (#{nearby_ids.join(', ')})
        AND NOT EXISTS(
          SELECT 1 FROM swiped_ons 
          WHERE swiped_ons.swiped_on_id = users.id 
          AND swiped_ons.user_id = #{id}
        )
        LIMIT #{n}"
      ])
      .map{|user| user.profile}
  end

  def inactive_mutual_matches
    User.joins(:match)
      .where('matches.active = ?', false)
      .where("matches.match_id = ?", self.id)
      .where("matches.user_id IN (?)", self.matches.pluck(:id))
      .order(created_at: :desc)
  end

  def mutual_matches
    # index 	active -> match_id -> user_id?
    User.joins(:match)
      .where('matches.active = ?', true)
      .where("matches.match_id = ?", self.id)
      .where("matches.user_id IN (?)", self.matches.pluck(:id))
      .order(created_at: :desc)
  end
  
  def mutual_matches_test
    User.joins({match: :conversation}, :images)
        .where('matches.active = ?', true)
        .where("matches.match_id = ?", self.id)
        .where("matches.user_id IN (?)", self.matches.pluck(:id))
        .where('images.image_key = 0 OR images.image_key = 3')
        .select('users.*, conversations.*, images.*')
        .as_json
  end

  def has_unseen_messages
    ActiveRecord::Base.connection.execute(
      "SELECT COUNT(*) FROM messages
      INNER JOIN conversations ON conversations.id = messages.conversation_id
      INNER JOIN user_conversations ON user_conversations.conversation_id = conversations.id
      WHERE user_conversations.user_id = #{id}
      AND messages.user_id != #{id}
      AND NOT EXISTS(
        SELECT 1 FROM seens
        WHERE seens.message_id = messages.id
        AND seens.user_id = #{id}
      )").getvalue(0,0) > 0
  end

  def filter_from_public_profile
    [
      'provider', 'uid', 'allow_password_change', 'nickname', 'image', 'email', 'created_at', 
      'updated_at', 'image_id', 'last_latitude', 'last_longitude', 'been_seen_by',
    ]
  end

  def match_profiles
    # this is what, 8 queries per match profile? That's a lot.
    # see if we can build on mutual_matches to load everything in fewer via joins
    # and then direct serialization instead of object instantiation
    # can probably do this better. Get user, then their convos, then those users
    # move to matches model...or conversation?
    mutual_matches.map do |match_user| 
      match = Match.find_by(user_id: self.id, match_id: match_user.id)
      convo = match.conversation


      user_convo = convo.user_conversations.find_by(user_id: self.id)

      match_user.profile_lite.merge(
        user_has_seen: user_convo.fetch_and_set_seen,
        messages: convo.messages.order('created_at desc'),
        match_date: convo.created_at,
        distance_from_user: match_user.distance_from(self, units: :kms),
      )
    end
  end
  
  def match_profile(match, match_user)
    convo = match.conversation
    user_convo = convo.user_conversations.find_by(user_id: self.id)
    match_user.profile_lite.merge(
      user_has_seen: user_convo.fetch_and_set_seen,
      messages: convo.messages.order('created_at desc'),
    )
  end

  def notifier
    @notifier ||= FcmPushNotifier.new(self)
  end

  def been_reported?
    @been_reported ||= Report.where(report_id: self.id).present?
  end
end
