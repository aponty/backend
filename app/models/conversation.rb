class Conversation < ApplicationRecord
  has_many :messages, dependent: :destroy

  has_many :user_conversations
  has_many :users, through: :user_conversations, dependent: :destroy

  belongs_to :match

  def as_json(options={})
    super({
      only: [:id, :match_id],
      includes: :messages,
    }.merge(options))
  end

  def last_message
    messages.includes(:seens).order(:id).last.as_json(methods: [:seen_by])
  end

  def serialized_messages
    messages.includes(:seens).order(created_at: :desc).to_json(methods: [:seen_by])
  end

  def chat_screen_mark_seen(seer_id)
    logger.info "chat screen mark messages seen called"
    UserConversation.where(user_id: seer_id, conversation_id: id).update_all(user_has_seen: true)
    mark_messages_seen(seer_id)
  end
  
  def unseen_messages(seer_id)
    Message.find_by_sql([
      "SELECT messages.* FROM messages 
      WHERE messages.conversation_id = #{id} 
      AND NOT EXISTS(
        SELECT 1 FROM seens 
        WHERE seens.message_id = messages.id 
        AND seens.user_id = #{seer_id}
      )"])
  end

  def mark_messages_seen(seer_id)
    time = Time.now
    Seen.upsert_all(
      unseen_messages(seer_id).map do |el| 
        {
          message_id: el.id,
          user_id: seer_id,
          created_at: time,
          updated_at: time,
        }
      end
    ) unless unseen_messages(seer_id).empty?
  end
  
end
