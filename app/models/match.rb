class Match < ApplicationRecord
    belongs_to :user
    belongs_to :match, :class_name => "User"

    has_one :conversation, dependent: :destroy
    
    acts_as_mappable  :lat_column_name => :last_latitude,
                      :lng_column_name => :last_longitude

    after_create :make_convo

    def conversation
        # ensures one conversation per mutual match, built on second match
        # will error/overflow if convo isn't created
        is_mutual? && super.blank? ? inverse_match.conversation : super
    end

    def is_mutual?
       inverse_match.present?
    end

    def deactivate
        self.update(active: false) && inverse_match.update(active: false)
    end

    def activate
        self.update(active: true) && inverse_match.update(active: true)
    end

    def make_convo
        if inverse_match.present?
            convo = Conversation.create(match_id: self.id)
            UserConversation.create(conversation_id: convo.id, user_id: self.user_id)
            UserConversation.create(conversation_id: convo.id, user_id: self.match_id)
        end
    end

    def inverse_match
        @inverse_match ||= Match.find_by(user_id: self.match_id, match_id: self.user_id)
    end
  end
