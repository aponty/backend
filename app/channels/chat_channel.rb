class ChatChannel < ApplicationCable::Channel

    def subscribed
      convo.chat_screen_mark_seen(current_user.id)
      stream_for convo
    end
    
    def speak(data)
      body = data['message']
      raise 'No conversation found!' if convo.blank?
      raise 'No message!' if body.blank?

      mess = Message.create!(
        conversation_id: convo.id,
        user_id: sender.id,
        body: body,
      )
    end

    def convo
      @convo ||= Match.where(
        user_id: params["user_id"], 
        match_id: params["match_id"]
      ).first.conversation
    end
    
    def sender
      @sender ||= User.find(sender_id)
    end

    def sender_id
      @sender_id ||= params["user_id"]
    end
  end
