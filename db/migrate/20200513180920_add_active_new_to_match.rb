class AddActiveNewToMatch < ActiveRecord::Migration[6.0]
  def change
    add_column :matches, :active, :boolean, default: true
    add_column :matches, :new, :boolean, default: true
  end
end
