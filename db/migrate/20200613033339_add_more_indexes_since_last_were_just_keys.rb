class AddMoreIndexesSinceLastWereJustKeys < ActiveRecord::Migration[6.0]
  def change
    add_index :user_conversations, [:id, :user_has_seen]

    add_index :swiped_ons, [:user_id, :swiped_on_id]
    add_index :swiped_ons, [:swiped_on_id, :user_id]

    add_index :seens, :user_id
    add_index :seens, :message_id

    add_index :reports, :user_id
    add_index :reports, :report_id

    add_index :matches, [:user_id, :match_id]
    add_index :matches, [:match_id, :user_id]

    remove_index :messages, name: "index_messages_on_created_at"
  end
end
