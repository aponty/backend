class ChangeLatLongToFloat < ActiveRecord::Migration[6.0]
  def change
    change_column :users, :last_latitude, :float
    change_column :users, :last_longitude, :float
  end
end
