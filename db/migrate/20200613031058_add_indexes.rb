class AddIndexes < ActiveRecord::Migration[6.0]
  def change
    # from_table, to_table
    add_index :images, :user_id

    add_foreign_key :reports, :users, column: :user_id
    add_foreign_key :reports, :users, column: :report_id

    add_foreign_key :seens, :users, column: :user_id
    add_foreign_key :seens, :messages, column: :message_id

    add_foreign_key :swiped_ons, :users, column: :user_id
    add_foreign_key :swiped_ons, :users, column: :swiped_on_id

    add_foreign_key :matches, :users, column: :user_id
    add_foreign_key :matches, :users, column: :match_id
  end
end
