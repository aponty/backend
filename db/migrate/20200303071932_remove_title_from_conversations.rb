class RemoveTitleFromConversations < ActiveRecord::Migration[6.0]
  def change
    remove_column :conversations, :title
  end
end
