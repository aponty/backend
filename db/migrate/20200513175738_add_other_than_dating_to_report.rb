class AddOtherThanDatingToReport < ActiveRecord::Migration[6.0]
  def change
    add_column :reports, :other_than_dating, :boolean, default: false
  end
end
