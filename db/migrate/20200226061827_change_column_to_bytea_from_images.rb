class ChangeColumnToByteaFromImages < ActiveRecord::Migration[6.0]
  def change
    change_column :images, :image, 'bytea USING CAST(image AS bytea)'
  end
end
