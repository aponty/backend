class AddBeenSwipedToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :been_seen_by, :integer, array: true, default: []
  end
end
