class AddSearchRadiusToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :search_radius, :float, default: 100.0
    add_index :users, :search_radius
  end
end
