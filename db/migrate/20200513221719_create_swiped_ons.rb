class CreateSwipedOns < ActiveRecord::Migration[6.0]
  def change
    create_table :swiped_ons do |t|
      t.integer :user_id
      t.integer :swiped_on_id

      t.timestamps
    end
  end
end
