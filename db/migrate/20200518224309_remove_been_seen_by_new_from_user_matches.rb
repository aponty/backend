class RemoveBeenSeenByNewFromUserMatches < ActiveRecord::Migration[6.0]
  def change

    remove_column :users, :been_seen_by, :integer

    remove_column :matches, :new, :boolean
  end
end
