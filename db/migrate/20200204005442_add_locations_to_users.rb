class AddLocationsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :last_latitude, :decimal, :precision => 10, scale: 6
    add_column :users, :last_longitude, :decimal, :precision => 10, :scale => 6
    add_index :users, [:last_latitude, :last_longitude]
  end
end
