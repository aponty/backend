class AddImageToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :images, :users, foreign_key: true, index: true
    add_foreign_key :images, :users
  end
end
