class RemoveUsersFromImages < ActiveRecord::Migration[6.0]
  def change
    remove_column :images, :users_id
  end
end
