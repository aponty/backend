class AddUserHasSeenToUserConversation < ActiveRecord::Migration[6.0]
  def change
    add_column :user_conversations, :user_has_seen, :boolean, default: false
  end
end
