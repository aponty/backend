class AddFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :description, :string
    add_column :users, :sketch_rating, :int
    add_column :users, :sport_grade, :string
    add_column :users, :boulder_grade, :string
    add_column :users, :trad_grade, :string
  end
end
