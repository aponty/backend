class AddForiegnKeyToConversations < ActiveRecord::Migration[6.0]
  def change
    add_reference :conversations, :match, index: true, foreign_key: true
  end
end
