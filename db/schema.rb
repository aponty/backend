# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_16_015912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "conversations", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "match_id"
    t.index ["match_id"], name: "index_conversations_on_match_id"
  end

  create_table "images", force: :cascade do |t|
    t.integer "image_key"
    t.binary "image"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.index ["user_id"], name: "index_images_on_user_id"
  end

  create_table "matches", force: :cascade do |t|
    t.integer "user_id"
    t.integer "match_id"
    t.datetime "created_at", precision: 6, default: -> { "now()" }, null: false
    t.datetime "updated_at", precision: 6, default: -> { "now()" }, null: false
    t.boolean "active", default: true
    t.index ["match_id", "user_id"], name: "index_matches_on_match_id_and_user_id"
    t.index ["user_id", "match_id"], name: "index_matches_on_user_id_and_match_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.bigint "user_id", null: false
    t.bigint "conversation_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "reports", force: :cascade do |t|
    t.integer "user_id"
    t.integer "report_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "other_than_dating", default: false
    t.index ["report_id"], name: "index_reports_on_report_id"
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "seens", force: :cascade do |t|
    t.integer "message_id"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["message_id"], name: "index_seens_on_message_id"
    t.index ["user_id"], name: "index_seens_on_user_id"
  end

  create_table "swiped_ons", force: :cascade do |t|
    t.integer "user_id"
    t.integer "swiped_on_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["swiped_on_id", "user_id"], name: "index_swiped_ons_on_swiped_on_id_and_user_id"
    t.index ["user_id", "swiped_on_id"], name: "index_swiped_ons_on_user_id_and_swiped_on_id"
  end

  create_table "user_conversations", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "conversation_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "user_has_seen", default: false
    t.index ["conversation_id"], name: "index_user_conversations_on_conversation_id"
    t.index ["id", "user_has_seen"], name: "index_user_conversations_on_id_and_user_has_seen"
    t.index ["user_id"], name: "index_user_conversations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.integer "sketch_rating"
    t.string "sport_grade"
    t.string "boulder_grade"
    t.string "trad_grade"
    t.string "tr_grade"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.float "last_latitude"
    t.float "last_longitude"
    t.integer "age", default: 10
    t.string "gender", default: "More"
    t.float "search_radius", default: 100.0
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["last_latitude", "last_longitude"], name: "index_users_on_last_latitude_and_last_longitude"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["search_radius"], name: "index_users_on_search_radius"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "conversations", "matches"
  add_foreign_key "images", "users"
  add_foreign_key "matches", "users"
  add_foreign_key "matches", "users", column: "match_id"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "reports", "users"
  add_foreign_key "reports", "users", column: "report_id"
  add_foreign_key "seens", "messages"
  add_foreign_key "seens", "users"
  add_foreign_key "swiped_ons", "users"
  add_foreign_key "swiped_ons", "users", column: "swiped_on_id"
  add_foreign_key "user_conversations", "conversations"
  add_foreign_key "user_conversations", "users"
end
