# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def encoded_spot_bot_image
    path = Rails.root.to_s + "/assets/spot_bot.png"
    spot_bot_image = File.open(path, 'rb') {|file| file.read }
    @encoded_spot_bot_image ||= Base64.strict_encode64(spot_bot_image)
end

def make_spot_bot
    spot_bot = User.create(
        email: "spot.catch.dev@gmail.com", 
        password: 'passwordpassword',
        name: "SpotBot",
        sketch_rating: 3,
        password_confirmation: 'passwordpassword',
        last_latitude: 40.000000,
        last_longitude: 73.000000,
        description: "Welcome to the app! To report users, please tap the upper right hand corner. If it's for datin', they'll get a lovely red banner like you see here! Happy swiping \u{1F642}"
    )

    [0, 1, 2].each do |image_key|
        Image.create(
            image: encoded_spot_bot_image,
            image_key: image_key,
            user_id: spot_bot.id,
        )
    end
    spot_bot
end

emails = [ 'a@a.com', 'b@b.com', 'c@c.com', 'd@d.com', 'e@e.com']
names = ["TEST USER 1", "TEST USER 2", "TEST USER 3", "TEST USER 4", "TEST USER 5"]
latitude = -55.239854, -147.897290
longitude = -147.897290
sketch_ratings = [ 1,2,3,4,5 ]

make_spot_bot

emails.each_with_index do |email, i|
    user = User.create(
        email: email, 
        password: 'password',
        name: names[i],
        sketch_rating: sketch_ratings.sample,
        password_confirmation: 'password',
        last_latitude: latitude,
        last_longitude: longitude,
    )
end

User.where(name: names).each do |user|
    next unless User.last.images.blank?
    [0, 1, 2].each do |image_key|
        Image.create(
            image: encoded_spot_bot_image,
            image_key: image_key,
            user_id: user.id,
        )
    end
end

first_user = User.find_by(email: 'a@a.com')
first_match_user = User.find_by(email: 'c@c.com')
second_match_user = User.find_by(email: 'e@e.com')

first_match = first_match_user.match.create(match_id: first_user.id)
second_match = second_match_user.match.create(match_id: first_user.id)

first_match_user.swiped_on.create(swiped_on_id: first_user.id)
second_match_user.swiped_on.create(swiped_on_id: first_user.id)

# rake db:drop;rake db:create;rake db:migrate;rake db:seed
